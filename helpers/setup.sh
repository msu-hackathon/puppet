#!/bin/bash

# fix for grub boat loader issue in vagrant
# https://github.com/mitchellh/vagrant/issues/289#issuecomment-11073577
sudo apt-get -y remove grub-pc
sudo apt-get -y install grub-pc
sudo grub-install /dev/sda # precaution
sudo update-grub

# basic package installs
sudo apt-get update
sudo apt-get install build-essential openssh-server git wget ruby1.9.3 rubygems1.9.1 -y

# use the right ruby
sudo update-alternatives --set ruby /usr/bin/ruby1.9.1

# install puppet
wget http://apt.puppetlabs.com/puppetlabs-release-precise.deb
sudo dpkg -i puppetlabs-release-precise.deb
sudo apt-get update
sudo apt-get install puppet -y

# upgrade everything (just in case we haven't)
sudo apt-get upgrade -y

# set the timezone
echo "America/New_York" | sudo tee /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata

# clone the repository
git clone https://gitlab.msu.edu/msu-hackathon/puppet.git /tmp/setup

chmod 0700 /tmp/setup/helpers/*.sh

# and do puppet stuff
cd /tmp/setup
sudo gem install bundler
sudo bundle install
librarian-puppet install
sudo puppet apply ubuntu.pp --modulepath=modules/

