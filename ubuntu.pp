# apt-get update at run
exec { 'update':
  command => '/usr/bin/apt-get update',
}
User['atomaka'] -> Exec['update'] -> Package <| |>

# vmware tools
class { 'vmwaretools':
  version       => '9.0.1-913578',
  archive_url   => 'http://broncobama.itservices.msu.edu/',
  archive_md5   => '6240174d177f93bea35c0b011112b981',
}

# sudo setup
include '::sudo'
sudo::conf { 'sudo':
  priority => 10,
  content  => "%sudo ALL=(ALL) NOPASSWD: ALL\n",
}

# apache setup
class { '::apache':
  default_vhost => false,
  mpm_module    => 'prefork',
  require       => User['hackathon'],
}
include '::apache::mod::php'
file { "/var/www/${::hostname}":
  ensure  => directory,
  owner   => 'hackathon',
  require => Class['apache'],
}
apache::vhost { "${::hostname}.itservices.msu.edu":
  port    => '80',
  docroot => "/var/www/${::hostname}/public",
}
$cname1 = generate('/tmp/setup/helpers/cnames.sh','1')
file { "/var/www/${cname1}":
  ensure  => directory,
  owner   => 'hackathon',
  require => Class['apache'],
}
apache::vhost { "${cname1}.itservices.msu.edu":
  port          => '80',
  docroot       => "/var/www/${cname1}/public",
  docroot_owner => 'hackathon',
  docroot_group => 'hackathon',
}
$cname2 = generate('/tmp/setup/helpers/cnames.sh','2')
file { "/var/www/${cname2}":
  ensure  => directory,
  owner   => 'hackathon',
  require => Class['apache'],
}
apache::vhost { "${cname2}.itservices.msu.edu":
  port          => '80',
  docroot       => "/var/www/${cname2}/public",
  docroot_owner => 'hackathon',
  docroot_group => 'hackathon',
}
$cname3 = generate('/tmp/setup/helpers/cnames.sh','3')
file { "/var/www/${cname3}":
  ensure  => directory,
  owner   => 'hackathon',
  require => Class['apache'],
}
apache::vhost { "${cname3}.itservices.msu.edu":
  port          => '80',
  docroot       => "/var/www/${cname3}/public",
  docroot_owner => 'hackathon',
  docroot_group => 'hackathon',
}
$cname4 = generate('/tmp/setup/helpers/cnames.sh','4')
file { "/var/www/${cname4}":
  ensure  => directory,
  owner   => 'hackathon',
  require => Class['apache'],
}
apache::vhost { "${cname4}.itservices.msu.edu":
  port          => '80',
  docroot       => "/var/www/${cname4}/public",
  docroot_owner => 'hackathon',
  docroot_group => 'hackathon',
}

# mysql setup
include '::mysql::server'
class { '::mysql::bindings':
  php_enable => true,
}
mysql::db { $::hostname:
  user     => $::hostname,
  password => '',
  host     => 'localhost',
  grant    => 'all',
}

# phpmyadmin setup (...)
include '::phpmyadmin'
phpmyadmin::server { 'default': }

# ruby
class { 'ruby':
  ruby_package     => 'ruby1.9.1-full',
  rubygems_package => 'rubygems1.9.1',
  gems_version     => 'latest',
}

# git
include '::git'

# user setup cleanup
user { 'setup': ensure => absent, }

# setup my account
user { 'atomaka':
  ensure     => 'present',
  groups     => ['sudo'],
  managehome => true,
}
file { '/home/atomaka/.ssh':
  ensure  => directory,
  owner   => 'atomaka',
  group   => 'atomaka',
  mode    => '0700',
  require => User['atomaka'],
}
file { '/home/atomaka/.ssh/authorized_keys':
  ensure  => present,
  owner   => 'atomaka',
  group   => 'atomaka',
  mode    => '0600',
  content => file('/tmp/setup/keys/atomaka'),
  require => File['/home/atomaka/.ssh'],
}

# setup generic account
user { 'hackathon':
  ensure   => 'present',
  password => sha1('hackathon'),
  groups   => ['sudo'],
}
